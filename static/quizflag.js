fetch('static/worldl.json')
  .then(r=>r.json())
  .then(r=>{
      let rnd = Math.floor(r.length*Math.random());
      document.getElementById('question').innerHTML = `
      Choose the flag of: ${r[rnd].name}
      `;

      let distractors = [];
      distractors.push(r[rnd].flag);
      distractors.push(r[23].flag);
      distractors.push(r[44].flag);
      distractors.push(r[90].flag);
      for(let d of distractors){
          let img = document.createElement('img');
          img.src = d;
          img.style.width = '100px';
          document.getElementById('question').append(img);
      }
  })