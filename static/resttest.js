document.getElementById('getbutton').onclick = ()=>{
    //Make a GET call
    let id = document.getElementById('getid').value;
    fetch(`/api/country/${id}`)
      .then(r=>r.json())
      .then(r=>{
          document.getElementById('getoutput').value=JSON.stringify(r);
      })
}

document.getElementById('deletebutton').onclick = ()=>{
    //Make a DELETE call
    let id = document.getElementById('deleteid').value;
    fetch(`/api/country/${id}`, {method:'DELETE'})
}


document.getElementById('postbutton').onclick = ()=>{
    //Assemble the payload
    let payload = {
        id: document.getElementById('postid').value,
        name: document.getElementById('postname').value,
        continent: document.getElementById('postcontinent').value,
        capital: document.getElementById('postcapital').value,
    }
    fetch(`/api/country/${payload.id}`, {
        method:'post',
        body: JSON.stringify(payload),
        headers:{'content-type':'application/json'}
    })
}
